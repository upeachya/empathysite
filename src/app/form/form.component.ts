import { Component, OnInit } from '@angular/core';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import {faLinkedin} from '@fortawesome/free-brands-svg-icons';
import {faFacebook} from '@fortawesome/free-brands-svg-icons';

import{Message} from '../message';
import{AppService} from '../app.service';


@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  location=faLocationArrow;
  linked=faLinkedin;
  fb=faFacebook;
 
 public newMsg:Message;
 messages=[];

  constructor(private _appService:AppService) { }

  ngOnInit() {
    this.newMsg={
      _id:'',
      name:'',
      phone:'',
      company:'',
      email:'',
      message:''
    }
  }

  public newSubmit(){
    this._appService.newMessage(this.newMsg)
      .subscribe(res=>{
        if (res.success==true){
        this.messages.push(res);
        }
      },
      error=>{
        console.log(error);
        alert(error._body)
      })
  }

}
