import { Component, OnInit } from '@angular/core';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import {faSkype} from '@fortawesome/free-brands-svg-icons'; 
import {faLinkedin} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'social-icons',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.css']
})
export class SocialComponent implements OnInit {
  facebook=faFacebook;
  skype=faSkype;
  linkedin=faLinkedin;
  constructor() { }

  ngOnInit() {
  } 

}
