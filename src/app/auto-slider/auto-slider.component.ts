import { Component, OnInit } from '@angular/core';
import { faImages } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'auto-slider',
  templateUrl: './auto-slider.component.html',
  styleUrls: ['./auto-slider.component.css']
})
export class AutoSliderComponent implements OnInit {

  constructor() { }
  


  ngOnInit(){
    window.onload=showSlides;
  }

  
}

function showSlides() {

  var slides =document.getElementsByClassName("mySlides") as HTMLCollectionOf<HTMLElement>;

  for (var i = 0; i <=slides.length-1; i++) {
      slides[i].style.display = "none";  
    }

  if(this.slideIndex<slides.length-1){
    this.slideIndex++;
  }
  else{
    this.slideIndex=0;
  }

  slides[this.slideIndex].style.display="block";
  setTimeout(showSlides,5000);
}

