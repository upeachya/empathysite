import { Injectable } from '@angular/core';
import{Http, Response,Headers, RequestOptions} from '@angular/http';
import {map} from 'rxjs/operators';
import{Message} from './message'

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private _postUrl='/contact';

  constructor(private _http:Http) { }

  newMessage(message:Message){
    let headers= new Headers({'Content-Type':'application/json'})
    let options=new RequestOptions({headers:headers});
    return this._http.post(this._postUrl,JSON.stringify(message),options)
      .pipe(map((response:Response)=>response.json()));
  }

}
