import { Component, OnInit } from '@angular/core';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'contact-icons',
  templateUrl: './contact-icons.component.html',
  styleUrls: ['./contact-icons.component.css']
})
export class ContactIconsComponent implements OnInit {

  constructor() { }
  location=faLocationArrow;
  phone=faPhone;
  mail=faEnvelope;

  ngOnInit() {
  }

  refresh(): void {
    window.location.reload();
}

}
