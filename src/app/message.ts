export class Message {
    _id:String;
    name:String;
    email:String;
    phone:String;
    message:String;
    company:String;
}