import { Component, OnInit } from '@angular/core';
import{faArrowUp} from '@fortawesome/free-solid-svg-icons';
import{faArrowDown} from '@fortawesome/free-solid-svg-icons';
import{faAtom} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent {
more: boolean=true;
up=faArrowUp;
down=faArrowDown;
atom=faAtom;

  constructor() { }
readmore(){
  this.more=!this.more;
}

less(){
  this.more=!this.more;
}


}
