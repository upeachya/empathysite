import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import{CommonModule,APP_BASE_HREF,LocationStrategy,HashLocationStrategy} from '@angular/common';

import { AppComponent } from './app.component';
import { RoutesComponent } from './routes/routes.component';
import { ResponsiveNavbarComponent } from './responsive-navbar/responsive-navbar.component';
import { NewsComponent } from './news/news.component';
import { TechnewsComponent } from './technews/technews.component';
import { TeamComponent } from './team/team.component';
import { ContactIconsComponent } from './contact-icons/contact-icons.component';
import { AutoSliderComponent } from './auto-slider/auto-slider.component';
import { AboutComponent } from './about/about.component';
import { ServicesComponent } from './services/services.component';
import { SocialComponent } from './social/social.component';
import { CareerComponent } from './career/career.component';
import { NewPartnerComponent } from './new-partner/new-partner.component';
import { FormComponent } from './form/form.component';
import { VacancyComponent } from './vacancy/vacancy.component';
import { FooterComponent } from './footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    RoutesComponent,
    ResponsiveNavbarComponent,
    NewsComponent,
    TechnewsComponent,
    TeamComponent,
    ContactIconsComponent,
    AutoSliderComponent,
    AboutComponent,
    ServicesComponent,
    SocialComponent,
    CareerComponent,
    NewPartnerComponent,
    FormComponent,
    VacancyComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    FontAwesomeModule,
    RouterModule.forRoot([ 
    {path: "", component: RoutesComponent}, 
    {path:"techNews", component:TechnewsComponent},
    {path:"news", component:NewsComponent},
    {path:"team", component:TeamComponent},
    {path:"contact",component:FormComponent},
    {path:"vacancy",component:VacancyComponent}
    ])
  ],
  providers: [
    {provide:APP_BASE_HREF,useValue:'/'},
    {provide:LocationStrategy,useClass:HashLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
