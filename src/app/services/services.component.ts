import { Component, OnInit } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  check=faCheck;
  frontEnd=["Angular", "React","Vue" ];
  backEnd=["PHP(Laravel-Backend framework)","Python(Django-Backend framework)","Ruby(Ruby on Rails-Backend framework)"];
  mobile=["Ionic(Hybrid mobile app)", "React Native(Hybrid mobile app)","Android Studio(Android app)" ];
  others=["MEAN(Fullstack JS framework)", "SQL(MySQL and PostgreSQL)", "C++,Java and Scala"];
  constructor() { }

  ngOnInit() {
  }

}
