import { Component } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons'


@Component({
  selector: 'responsive-navbar',
  templateUrl: './responsive-navbar.component.html',
  styleUrls: ['./responsive-navbar.component.css']
})
export class ResponsiveNavbarComponent {
  navbarOpen = false;
  bars=faBars;

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }
  refresh(): void {
    window.location.reload();
}
  constructor() { }

 }
