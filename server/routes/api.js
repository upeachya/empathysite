const express=require('express');
const router=express.Router(); 
const nodemailer=require('nodemailer');

const Message=require('../models/message')

const mongoose=require('mongoose');
mongoose.connect('mongodb://localhost/empathy', {useNewUrlParser:true}) //.................see here
    .then(()=>console.log('connected to mongodb...'))
    .catch(()=>console.error('Error: ',error))


// router.get('/allMessage',function(req,res){
//     Message.find({})
//         .exec(function(error,entries){
//             if (error){
//                 console.log(error);
//             }else{
//                 res.json(entries);
//             }
//         })
// })

//........................................................................................
router.post('/contact',async function(req,res){
        var newEntry=new Message();
        
        newEntry.name=req.body.name;
        newEntry.mobile=req.body.mobile;
        newEntry.email=req.body.email; 
        newEntry.message=req.body.message;
        newEntry.company=req.body.company;
    
        newEntry
        .save(function(err,ent){
            if(err){
                console.log(err.message)
                res.json({message:err}).status(400)
            }else{
                console.log('ENTERED')
                //............................mail sending part
                const output=`
                <p>You have a new contact request</p>
                <ul>
                    <li>Name: ${req.body.name}</li>
                    <li>Company: ${req.body.company}</li>
                    <li>Email: ${req.body.email}</li>
                    <li>Phone: ${req.body.phone}</li>
                </ul>
                <h3>Message</h3>
                <p>${req.body.message}</p>
                `
                var transporter = nodemailer.createTransport({
                    service: 'gmail',
                    auth: {
                           user: 'clubempathyit@gmail.com',
                           pass: 'empathy123'
                       }
                   });
                
                   const mailOptions = {
                    from: 'clubempathyit@gmail.com', // sender address
                    to: 'clubempathyit@gmail.com', // list of receivers
                    subject: 'New message', // Subject line
                    html: output// plain text body
                  };
                  transporter.sendMail(mailOptions, function (err, info) {
                    if(err)
                      console.log(err)
                    else
                      console.log(info);
                 });
                //............................ends
               
                res.json(ent) 
            }
        })
    })

module.exports=router;