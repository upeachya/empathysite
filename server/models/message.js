const mongoose=require('mongoose');
const Schema=mongoose.Schema;

const messageSchema=new Schema({
    email:String,
    name:String,
    phone:String,
    company:String,
    message:String
});
 
module.exports=mongoose.model('message' ,messageSchema,'messages') 