const express=require('express');
const app=express();
const port=3000;
const bodyParser=require('body-parser');
const path=require('path');
const api=require('./server/routes/api');

app.use(express.static(path.join(__dirname,'dist/empathy')));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/',api);


app.get('*',(req,res)=>{
    res.sendFile(path.join(__dirname,'dist/empathy/index.html'))
})

app.listen(port,function(){
    console.log("Server is running on port "+port)
})

